package com.zero2oneit.mall.common.bean.goods;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;

import java.util.Date;

/**
 * Description: 商品信息表（社区）
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-02-22
 */

/**
 * indexName : 索引名称
 * shards ：分片
 * replicas ：副本
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Setting(settingPath = "enterprise-setting.json")
@Document(indexName = "products", shards = 1, replicas = 0)
public class Products{

	/**
	 * 商品id,水平拆分
	 */
	@Id
	private Long id;
	/**
	 * 商家id、水平拆分
	 */
	@Field(type = FieldType.Keyword)
	private Long businessId;
	/**
	 * 商家名称
	 */
	@Field(type = FieldType.Keyword)
	private String businessName;
	/**
	 * 商品类型id
	 * fielddata = true 表示不分词
	 */
	@Field(type = FieldType.Keyword, fielddata = true)
	private Long typeId;
	/**
	 * 商品类型名称
	 */
	@Field(type = FieldType.Keyword, index = false)
	private String typeName;
	/**
	 * 商品名称
	 */
	@Field(type = FieldType.Text,analyzer = "ik_smart",searchAnalyzer = "ik_max_word")
	private String productName;
	/**
	 * 商品条形码
	 */
	@Field(index = false)
	private String barCode;
	/**
	 * 商品预警值
	 */
	@Field(index = false)
	private Integer warningValue;
	/**
	 * 品牌id
	 */
	@Field(type = FieldType.Keyword)
	private Long brandId;
	/**
	 * 品牌名称
	 */
	@Field(type = FieldType.Keyword, index = false)
	private String brandName;
	/**
	 * 商品规格
	 */
	@Field(type = FieldType.Keyword, index = false)
	private String skuName;
	/**
	 * 商品单位
	 */
	@Field(index = false)
	private String productUnit;
	/**
	 * 商品重量
	 */
	@Field(index = false)
	private String productWeight;
	/**
	 * 商品重量单位
	 */
	@Field(index = false)
	private String productWeightUnit;
	/**
	 * 商品生产地
	 */
	@Field(index = false)
	private String productPlace;
	/**
	 * 商品主图路径
	 */
	@Field(index = false)
	private String mainPicture;
	/**
	 * 商品详情图路径
	 */
	@Field(index = false)
	private String detailPicture;
	/**
	 * 白底图片（浏览图片）路径
	 */
	@Field(index = false)
	private String whitePicture;
	/**
	 * 实物图片路径
	 */
	@Field(index = false)
	private String physicalPicture;
	/**
	 * 商品状态：0-审核中 1-审核通过||上架 2-审核不通过 3-违规 4-下架 5-删除
	 */
	@Field(type = FieldType.Keyword, index = false)
	private Integer productStatus;
	/**
	 * 短视频地址
	 */
	@Field(index = false)
	private String videoSrc;
	/**
	 * 排序
	 */
	@Field(type = FieldType.Keyword, index = false)
	private Integer productSort;
	/**
	 * 限购数量
	 */
	@Field(index = false)
	private Integer buyLimit;
	/**
	 * 生产日期
	 */
	@Field(index = false)
	private Date produceTime;
	/**
	 * 保质期
	 */
	@Field(index = false)
	private String lifeTime;
	/**
	 * 保质期单位
	 */
	@Field(index = false)
	private String lifeTimeUnit;
	/**
	 * 存储条件
	 */
	@Field(index = false)
	private String storeCondition;
	/**
	 * 模块ID
	 * fielddata = true 表示不分词
	 */
	@Field(type = FieldType.Keyword, fielddata = true)
	private Integer moudleId;

	@Field(type = FieldType.Keyword, index = false)
	ProductsRelation relation;

	/**
	 * 秒杀开始时间
	 */
	@Field(index = false)
	private Integer startTime;

}
