package com.zero2oneit.mall.common.query.member;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zero2oneit.mall.common.utils.query.QueryObject;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-23
 */
@Data
public class MemberCouponQueryObject extends QueryObject {

    private String memberId;

}
