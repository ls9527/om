package com.zero2oneit.mall.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zero2oneit.mall.common.bean.goods.NewcomersRule;
import com.zero2oneit.mall.common.query.goods.NewcomersRuleQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-06-02
 */
public interface NewcomersRuleService extends IService<NewcomersRule> {

    BoostrapDataGrid pageList(NewcomersRuleQueryObject qo);

    R status(String id, Integer status);
}

