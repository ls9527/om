package com.zero2oneit.mall.member.api;

import com.alibaba.fastjson.JSON;
import com.zero2oneit.mall.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: 地址管理
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021/4/18
 */
@RestController
@RequestMapping("/api/member/coupon")
public class CouponApi {

    @Autowired
    private StringRedisTemplate redisTemplate;

    /**
     * 加载优惠券
     * @return
     */
    @RequestMapping("/load")
    public R load(){
        String coupons = redisTemplate.opsForValue().get("om:coupon:list");
        return R.ok(JSON.toJSON(coupons));
    }

}
