package com.zero2oneit.mall.member.api;

import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.member.dto.PrizeDTO;
import com.zero2oneit.mall.member.service.PrizeRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021/5/24
 */
@RestController
@RequestMapping("/api/auth/member/prize")
public class PrizeApi {

    @Autowired
    private PrizeRecordService prizeRecordService;

    /**
     * 会员抽奖
     * @param
     * @return
     */
    @PostMapping("/draw")
    public R draw(@RequestBody PrizeDTO prizeDTO){
        return prizeRecordService.draw(prizeDTO);
    }


}
