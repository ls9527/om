package com.zero2oneit.mall.member.api;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zero2oneit.mall.common.bean.member.MemberSign;
import com.zero2oneit.mall.common.bean.member.MemberSignRecord;
import com.zero2oneit.mall.common.bean.member.MemberSignRule;
import com.zero2oneit.mall.common.query.member.MemberSignRecordQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.member.service.MemberSignRecordService;
import com.zero2oneit.mall.member.service.MemberSignRuleService;
import com.zero2oneit.mall.member.service.MemberSignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Description:
 *
 * @author Cmx
 * @date 2021/5/13 12:10
 */
@RestController
@RequestMapping("/api/auth/member/sign")
public class SignApi {

    @Autowired
    private MemberSignService memberSignService;

    @Autowired
    private MemberSignRuleService signRuleService;

    @Autowired
    private MemberSignRecordService signRecordService;

    /**
     * 加载会员规则和个人签到信息
     * @param memberId 参数
     * @return
     */
    @PostMapping("/load")
    public R load(@RequestBody Long memberId){
        QueryWrapper<MemberSignRule> wrapper = new QueryWrapper();
        MemberSignRule signRule = signRuleService.getOne(wrapper);
        QueryWrapper<MemberSign> wrappers = new QueryWrapper();
        wrappers.eq("member_id", memberId);
        MemberSign memberSign = memberSignService.getOne(wrappers);
        Map map = new HashMap();
        map.put("rule", signRule);
        map.put("sign", memberSign != null ? memberSign : JSON.parse("{\"id\":null,\"memberId\":\""+memberId+"\",\"signTime\":null,\"signCount\":0,\"signAmount\":0,\"signPoints\":0}"));
        return R.ok("加载成功", map);
    }

    /**
     * 查询签到明细
     * @param qo 参数
     * @return
     */
    @PostMapping("/list")
    public R list(@RequestBody MemberSignRecordQueryObject qo){
        BoostrapDataGrid page = signRecordService.recordList(qo);
        return R.ok("加载成功", page);
    }

    /**
     * 签到
     * @param qo 参数
     * @return
     */
    @PostMapping("/sign")
    public R list(@RequestBody MemberSign qo){
        System.out.println("qo = " + qo);
        memberSignService.saveOrUpdate(qo);
        signRecordService.save(new MemberSignRecord(null, qo.getMemberId(), qo.getSignTime(), qo.getPoint()));
        return R.ok("签到成功");
    }

}
