package com.zero2oneit.mall.member.controller;

import com.zero2oneit.mall.common.bean.member.PrizeRule;
import com.zero2oneit.mall.common.query.member.PrizeRuleQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zero2oneit.mall.member.service.PrizeRuleService;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-24
 */
@RestController
@RequestMapping("/admin/member/prizeRule")
public class PrizeRuleController {

    @Autowired
    private PrizeRuleService prizeRuleService;

    /**
     * 查询抽奖规则列表信息
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody PrizeRuleQueryObject qo){
        return prizeRuleService.ruleList(qo);
    }

    /**
     * 添加或编辑抽奖规则信息
     * @param prizeRule
     * @return
     */
    @PostMapping("/addOrEdit")
    public R addOrEdit(@RequestBody PrizeRule prizeRule){
        prizeRuleService.saveOrUpdate(prizeRule);
        return R.ok();
    }



}
