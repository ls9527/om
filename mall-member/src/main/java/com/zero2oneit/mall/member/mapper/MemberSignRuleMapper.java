package com.zero2oneit.mall.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zero2oneit.mall.common.bean.member.MemberSignRule;
import com.zero2oneit.mall.common.query.member.MemberSignRuleQueryObject;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author Tg
 * @create 2021-05-13
 * @description
 */
@Mapper
public interface MemberSignRuleMapper extends BaseMapper<MemberSignRule> {

    int selectTotal(MemberSignRuleQueryObject qo);

    List<Map<String, Object>> selectAll(MemberSignRuleQueryObject qo);

}
