package com.zero2oneit.mall.member.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zero2oneit.mall.common.bean.market.Advert;
import com.zero2oneit.mall.common.bean.member.CouponRule;
import com.zero2oneit.mall.common.query.member.CouponRuleQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.member.mapper.CouponRuleMapper;
import com.zero2oneit.mall.member.service.CouponRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-03-19
 */
@Service
public class CouponRuleServiceImpl extends ServiceImpl<CouponRuleMapper, CouponRule> implements CouponRuleService {

    @Autowired
    private CouponRuleMapper couponRuleMapper;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public BoostrapDataGrid pageList(CouponRuleQueryObject qo) {
        int total = couponRuleMapper.selectTotal(qo);
        return new BoostrapDataGrid(total, total ==0 ? Collections.EMPTY_LIST : couponRuleMapper.selectAll(qo));
    }

    @Override
    public R status(String id, Integer status) {
        CouponRule couponRule = new CouponRule();
        couponRule.setId(Long.valueOf(id));
        if (status==1){
            couponRule.setRuleStatus(2);
        }else if (status==2){
            couponRule.setRuleStatus(1);
        }
        int i = couponRuleMapper.updateById(couponRule);

        //根据类型查询设置到中奖
        QueryWrapper<CouponRule> wrapper = new QueryWrapper();
        wrapper.eq("rule_status", 1);
        List<CouponRule> couponRules = couponRuleMapper.selectList(wrapper);
        redisTemplate.opsForValue().set("om:coupon:list", JSON.toJSONString(couponRules));

        return i>0? R.ok():R.fail();
    }
}