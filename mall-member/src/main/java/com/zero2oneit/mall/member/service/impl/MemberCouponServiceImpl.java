package com.zero2oneit.mall.member.service.impl;

import com.zero2oneit.mall.common.bean.member.MemberCoupon;
import com.zero2oneit.mall.common.query.member.MemberCouponQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.zero2oneit.mall.member.mapper.MemberCouponMapper;
import com.zero2oneit.mall.member.service.MemberCouponService;

import java.util.Collections;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-23
 */
@Service
public class MemberCouponServiceImpl extends ServiceImpl<MemberCouponMapper, MemberCoupon> implements MemberCouponService {

    @Autowired
    private MemberCouponMapper memberCouponMapper;

    @Override
    public BoostrapDataGrid pageList(MemberCouponQueryObject qo) {
        //查询总记录数
        int total = memberCouponMapper.selectTotal(qo);
        return new BoostrapDataGrid(total, total == 0 ? Collections.EMPTY_LIST : memberCouponMapper.selectRows(qo));
    }

}