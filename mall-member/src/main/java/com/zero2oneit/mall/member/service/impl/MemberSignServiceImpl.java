package com.zero2oneit.mall.member.service.impl;

import com.zero2oneit.mall.common.bean.member.MemberSign;
import com.zero2oneit.mall.common.query.member.MemberSignQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.zero2oneit.mall.member.mapper.MemberSignMapper;
import com.zero2oneit.mall.member.service.MemberSignService;

import java.util.Collections;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-13
 */
@Service
public class MemberSignServiceImpl extends ServiceImpl<MemberSignMapper, MemberSign> implements MemberSignService {

    @Autowired
    private MemberSignMapper memberSignMapper;

    @Override
    public BoostrapDataGrid signList(MemberSignQueryObject qo) {
        int total = memberSignMapper.selectTotal(qo);
        return new BoostrapDataGrid(total, total ==0 ? Collections.EMPTY_LIST : memberSignMapper.selectAll(qo));
    }
}