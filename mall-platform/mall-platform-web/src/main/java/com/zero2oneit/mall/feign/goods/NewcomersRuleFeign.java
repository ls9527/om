package com.zero2oneit.mall.feign.goods;

import com.zero2oneit.mall.common.bean.goods.NewcomersRule;
import com.zero2oneit.mall.common.query.goods.NewcomersRuleQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021/2/5
 */
@FeignClient("goods-service")
public interface NewcomersRuleFeign {

    @PostMapping("/admin/newcomers/list")
    BoostrapDataGrid list(@RequestBody NewcomersRuleQueryObject qo);

    @PostMapping("/admin/newcomers/addOrEdit")
    R addOrEdit(@RequestBody NewcomersRule newcomersRule);

    @PostMapping("/admin/newcomers/delByIds")
    R delByIds(@RequestBody String ids);

    @PostMapping("/admin/newcomers/status")
    R status(@RequestParam("id") String id, @RequestParam("status") Integer status);
}
