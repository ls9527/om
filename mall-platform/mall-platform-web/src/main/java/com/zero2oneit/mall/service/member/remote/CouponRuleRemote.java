package com.zero2oneit.mall.service.member.remote;

import com.zero2oneit.mall.common.annotion.OperateLog;
import com.zero2oneit.mall.common.bean.member.CouponRule;
import com.zero2oneit.mall.common.enums.BusinessType;
import com.zero2oneit.mall.common.query.member.CouponRuleQueryObject;
import com.zero2oneit.mall.common.utils.Assert;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.feign.member.CouponRuleFeign;
import com.zero2oneit.mall.feign.oss.OssFeign;
import com.zero2oneit.mall.system.base.utils.UserContext;
import com.zero2oneit.mall.system.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Description: 远程调用商品分类（社区团购）服务
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021/2/5
 */
@RestController
@RequestMapping("/remote/coupon")
@CrossOrigin
public class CouponRuleRemote {

    @Autowired
    private CouponRuleFeign ruleFeign;

    @Autowired
    private OssFeign ossFeign;

    /**
     * 查询优惠劵规则列表信息
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody CouponRuleQueryObject qo){
        return ruleFeign.list(qo);
    }

    /**
     * 添加或编辑优惠劵规则信息
     * @param couponRule
     * @return
     */
    @OperateLog(title = "添加或编辑优惠劵规则信息", businessType = BusinessType.UPDATE)
    @PostMapping("/addOrEdit")
    public R addOrEdit(CouponRule couponRule, @RequestParam("file") MultipartFile[] file){
        String imgUrl = null;
        if (file != null && file.length >= 1) {
            imgUrl = ossFeign.uploadImage(file, "advert");
            if(couponRule.getId() == null){
                couponRule.setPicUrl(imgUrl);
            }else {
                couponRule.setPicUrl(imgUrl + (couponRule.getPicUrl().length() > 0 ? ","+couponRule.getPicUrl() : ""));
            }
        }

        User user = UserContext.getCurrentUser(UserContext.SYS_USER);
        couponRule.setOperatorName(user.getUsername());
        //添加或编辑广告信息
        return ruleFeign.addOrEdit(couponRule);
    }

    /**
     * 删除优惠劵规则信息
     * @param ids
     * @return
     */
    @OperateLog(title = "删除优惠劵规则信息", businessType = BusinessType.DELETE)
    @PostMapping("/delByIds")
    public R delByIds(String ids, String urls){
        Assert.notNull(ids, "请选择操作项");
        ruleFeign.delByIds(ids);
//        ossFeign.deleteImage(urls);
        return R.ok();
    }


    /**
     * 更改优惠劵规则信息状态
     * @param id
     * @param status
     * @return
     */
    @PostMapping("/status")
    public R status(String id, Integer status){
        return ruleFeign.status(id, status);
    }

}
